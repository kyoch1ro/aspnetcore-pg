﻿using IdentityServerCommon.Resources;
using LibraryWebSrvc.Helpers;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryWebSrvc.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticateController : ControllerBase
    {
        private readonly string _base = @"https://localhost:44347"; //move to configuration

        [HttpPost]
        [Route("login")]
        public async Task<object> Login([FromBody] LoginResource resource)
        {
            return await ApiProxy.Send(_base, Request, HttpContext, resource);
        }

        [HttpPost("{refreshToken}/refresh")]
        public async Task<object> RefreshToken([FromRoute]string refreshToken)
        {
            return await ApiProxy.Send(_base, Request, HttpContext);
        }
    }
}