﻿using BooksCommon.Resources;
using LibraryWebSrvc.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryWebSrvc.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class BooksController : ControllerBase
    {
        private readonly string _base = @"https://localhost:44328"; // move to app configuration

        [HttpGet("")]
        public async Task<object> GetBooks()
        {
            return await ApiProxy.Send(_base, Request, HttpContext);
        }

        [HttpGet("{bookId}")]
        public async Task<object> GetBook(int bookId)
        {
            return await ApiProxy.Send(_base, Request, HttpContext);
        }



        [HttpPost("")]
        public async Task<object> StoreBook([FromBody] BookResource resource)
        {
            return await ApiProxy.Send(_base, Request, HttpContext, resource);
        }



        [HttpPut("{bookId}")]
        public async Task<object> UpdateBook(int bookId, [FromBody] BookResource resource)
        {
            return await ApiProxy.Send(_base, Request, HttpContext, resource);
        }



        [HttpDelete("{bookId}")]
        public async Task<object> DeleteBook(int bookId)
        {
            return await ApiProxy.Send(_base, Request, HttpContext);
        }

    }
}