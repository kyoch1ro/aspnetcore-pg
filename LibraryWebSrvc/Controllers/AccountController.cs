﻿using IdentityServerCommon.Resources;
using LibraryWebSrvc.Helpers;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryWebSrvc.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly string _base = @"https://localhost:44347"; // move to configuration
        [HttpPost]
        [Route("register")]
        public async Task<object> Register([FromBody] RegistrationResource resource)
        {
            return await ApiProxy.Send(_base, Request, HttpContext, resource);
        }
    }
}