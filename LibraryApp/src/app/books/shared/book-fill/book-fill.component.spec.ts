import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookFillComponent } from './book-fill.component';

describe('BookFillComponent', () => {
  let component: BookFillComponent;
  let fixture: ComponentFixture<BookFillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookFillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookFillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
