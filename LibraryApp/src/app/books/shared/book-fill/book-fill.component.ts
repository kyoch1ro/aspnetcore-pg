import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { BookResource } from '../../resources/book-resource';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BehaviorSubject, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-book-fill',
  templateUrl: './book-fill.component.html',
  styleUrls: ['./book-fill.component.css']
})
export class BookFillComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  private resource = new BehaviorSubject<BookResource>(null);

  @Output() Submitted = new EventEmitter<BookResource>();
  @Input() set Resource(val) {
    this.resource.next(val);
  }

  get Resource() {
    return this.resource.getValue();
  }

  IsSubmitted = false;
  form: FormGroup;
  
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      id: [0],
      name: ['', Validators.required],
      publishedDate: [null, Validators.required]
    });

    this.PatchForm();
  }

  private PatchForm() {
    const sub = this.resource
                    .pipe(filter(x => x != null))
                    .subscribe(x => this.form.patchValue(this.Resource));
    this.subscriptions.push(sub);
  }

  onSubmit() {
    this.IsSubmitted = true;
    if (this.form.invalid) { return; }
    this.Submitted.next(this.form.value);
  }
  get f() { return this.form.controls; }

  ngOnDestroy(): void {
    this.subscriptions.forEach(x => x.unsubscribe());
  }
}
