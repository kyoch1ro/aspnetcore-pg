import { Component, OnInit } from '@angular/core';
import { BookService } from '../services/book.service';
import { Router } from '@angular/router';
import { BookResource } from '../resources/book-resource';
import { AlertService } from 'src/app/core/services/alert.service';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpErrorHelper } from 'src/app/core/helpers/http-error-helper';

@Component({
  selector: 'app-book-create',
  templateUrl: './book-create.component.html',
  styleUrls: ['./book-create.component.css']
})
export class BookCreateComponent implements OnInit {
  IsLoading = false;
  constructor(private bookSrvc: BookService,
              private router: Router,
              private alertSrvc: AlertService) { }

  ngOnInit() {
  }

  StoreBook(item: BookResource) {
    this.IsLoading = true;
    this.bookSrvc
      .Store(item)
      .subscribe(x => {
        this.IsLoading = false;
        this.alertSrvc.CreateNotification('Book successfully created.');
        this.router.navigate(['/books']);
      }, (err: HttpErrorResponse)  => {
        this.IsLoading = false;
        this.alertSrvc.AlertHttpError(err);
      });
  }

}
