import { Component, OnInit } from '@angular/core';
import { BookResource } from '../resources/book-resource';
import { BookService } from '../services/book.service';
import { SimpleListModel } from 'src/app/shared/components/simple-list-model';
import { ThrowStmt } from '@angular/compiler';
import { tap } from 'rxjs/operators';
import { AlertService } from 'src/app/core/services/alert.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {
  items: SimpleListModel[] = [];
  IsLoading = false;
  constructor(private bookSrvc: BookService,
              private alertSvrc: AlertService,
              private router: Router) { }

  ngOnInit() {
    this.RefreshList();
  }

  RefreshList() {
    this.IsLoading = true;
    this.bookSrvc
      .List()
      .subscribe(x => {
        this.IsLoading = false;
        this.items = x.map(y => this.MapToSimpleList(y));
      });
  }

  ItemClicked(item: SimpleListModel) {
    this.router.navigate(['/books','edit', item.obj.id])
  }

  ItemDeleted(item: SimpleListModel) {
    this.bookSrvc
        .Delete((item.obj as BookResource).id)
        .pipe(tap(
          x => this.alertSvrc.CreateNotification('Item successfully deleted')
        ))
        .subscribe(x => this.RefreshList());
  }



  private MapToSimpleList(x: BookResource): SimpleListModel {
    return {
      obj: x,
      title: x.name
    };
  }

}
