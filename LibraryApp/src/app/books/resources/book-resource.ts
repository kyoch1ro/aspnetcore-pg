export class BookResource {
    id: number;
    name: string;
    publishedDate: Date;
}
