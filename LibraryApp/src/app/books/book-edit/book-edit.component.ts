import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { BookResource } from '../resources/book-resource';
import { Router, ActivatedRoute } from '@angular/router';
import { BookService } from '../services/book.service';
import { AlertService } from 'src/app/core/services/alert.service';

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.css']
})
export class BookEditComponent implements OnInit {
  IsLoading = true;
  Caption = 'Loading...';
  private resource = new BehaviorSubject<BookResource>(null);

  get BookId() {
    return this.route.snapshot.paramMap.get('bookId');
  }
  get Resource() {
    return this.resource.getValue();
  }

  set Resource(val) {
    this.resource.next(val);
  }


  constructor(private router: Router,
              private route: ActivatedRoute,
              private bookSrvc: BookService,
              private alertSrvc: AlertService) { }

  ngOnInit() {
    this.bookSrvc.Get(+this.BookId)
        .subscribe(x => {
          this.Resource = x;
          this.IsLoading = false;
        });
  }

  UpdateBook(data: BookResource) {
    this.IsLoading = true;
    this.bookSrvc.Update(+this.BookId, data)
    .subscribe(x => {
      this.IsLoading = false;
      this.alertSrvc.CreateNotification('Data successfully updated');
    }, err => {
      this.IsLoading = false;
      this.alertSrvc.AlertHttpError(err);
    });
  }

}
