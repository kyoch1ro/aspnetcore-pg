import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { DataService } from 'src/app/core/services/data-service';
import { BookResource } from '../resources/book-resource';

@Injectable({
  providedIn: 'root'
})
export class BookService extends DataService<BookResource, number> {
  constructor(http: HttpClient) {
    super(`${environment.webService}/api/books`, http);
  }
}
