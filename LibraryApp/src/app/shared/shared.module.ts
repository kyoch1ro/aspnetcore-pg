import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimpleListComponent } from './components/simple-list/simple-list.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AsyncContainerComponent } from './components/async-container/async-container.component';


@NgModule({
  declarations: [SimpleListComponent, AsyncContainerComponent],
  imports: [
    CommonModule,
    ModalModule
  ],
  exports: [ SimpleListComponent, AsyncContainerComponent ]
})
export class SharedModule { }
