import { Component, OnInit, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-async-container',
  templateUrl: './async-container.component.html',
  styleUrls: ['./async-container.component.css']
})
export class AsyncContainerComponent implements OnInit {
  private isLoading = new BehaviorSubject<boolean>(false);
  @Input() set IsLoading(val) {
    this.isLoading.next(val);
  }
  get IsLoading() {
    return this.isLoading.getValue();
  }
  @Input() Caption = 'Loading please wait...';
  constructor() { }

  ngOnInit() {
  }

}
