import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SimpleListModel } from '../simple-list-model';

@Component({
  selector: 'app-simple-list',
  templateUrl: './simple-list.component.html',
  styleUrls: ['./simple-list.component.css']
})
export class SimpleListComponent implements OnInit {
  @Input() items: SimpleListModel[] = [];
  @Input() showDelete = true;
  @Output() Clicked = new EventEmitter<SimpleListModel>();
  @Output() Deleted = new EventEmitter<SimpleListModel>();
  constructor() { }

  ngOnInit() {
  }

  ItemClicked(item: SimpleListModel) {
    this.Clicked.next(item);
  }

  ItemDeleted(event: Event, item: SimpleListModel) {
    event.stopPropagation();
    this.Deleted.next(item);
  }

}
