import { Component, OnInit } from '@angular/core';
import { AlertService } from './core/services/alert.service';
import { NotificationType } from './core/models/notification-type';
import { Router } from '@angular/router';
import { AuthService } from './core/services/auth.service';
import { distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private alertSrvc: AlertService,
              private router: Router,
              private authSrvc: AuthService) {
  }
  ngOnInit(): void {
    this.authSrvc
    .IsAuthenticated$
    .pipe(distinctUntilChanged())
    .subscribe(x => {
      if (x === true) {
        this.router.navigate(['/books']);
      } else {
        this.router.navigate(['/login']);
      }
    });
    this.authSrvc.CheckCachedToken();

  }

}
