import { Component, OnInit } from '@angular/core';
import { AlertService } from '../services/alert.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { take } from 'rxjs/operators';
import { NotificationType } from '../models/notification-type';
import { BaseForm } from '../models/base-form';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent extends BaseForm implements OnInit {

  constructor(private authSvc: AuthService,
              private fb: FormBuilder,
              private alertSvc: AlertService) {
    super();
  }

  ngOnInit() {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }


  onSubmit() {
    this.IsSubmitted = true;
    if (this.form.invalid) { return; }

    this.IsLoading = true;
    this.authSvc
        .Login(this.form.get('username').value, this.form.get('password').value)
        .pipe(take(1))
        .subscribe( () => {},
        err => {
          this.alertSvc.CreateNotification('Invalid username or password', NotificationType.danger);
          this.IsLoading = false;
        });
  }

  // convenience getter for easy access to form fields
 

}
