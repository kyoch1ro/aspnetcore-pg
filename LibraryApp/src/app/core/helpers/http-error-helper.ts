import { HttpErrorResponse } from '@angular/common/http';

export class HttpErrorHelper {
    public static ExtractErrors(error: HttpErrorResponse) {
        console.log(error);

        const err = this.extractErrorObject(error);
        const items: PropertyError[] = [];
        if (!err) {
            items.push({
                property: '',
                errors: [
                    'Server error, please contact your administrator'
                ]
            });
            return items;
        }
        Object.keys(err).forEach(key => {
            items.push({
                errors: err[key],
                property: key
            });
        });
        return items.map(x => {
            if (x.property.startsWith('$.')) {
                x.property = x.property.replace('$.', '');
            }
            x.property = x.property.charAt(0).toLowerCase() + x.property.slice(1);
            return x;
        });
    }


    private static extractErrorObject(err: HttpErrorResponse) {


        if (err.error.errors) {
            return err.error.errors;
        }

        if (err.error) {
            const x = JSON.parse(err.error);
            if (x.errors) {
                return x.errors;
            }
            return x;
        }
        return {};
    }


}



export class PropertyError {
    public property: string;
    public errors: string[];
}