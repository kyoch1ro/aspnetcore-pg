import { NotificationType } from './notification-type';

export class Notification {
    id: string;
    message: string;
    type: NotificationType;

    constructor(obj: any) {
        this.id = obj && obj.id || this.GenerateNotifId(16);
        this.message = obj && obj.message || '';
        this.type = obj && obj.type || NotificationType.success;
    }


    private GenerateNotifId(len) {
        const chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        let result = '';
        let rand;
        for (let i = 0; i < len; i++) {
          rand = Math.floor(Math.random() * chars.length);
          result += chars.charAt(rand);
        }
        return result;
    }
}
