import { FormGroup } from '@angular/forms';

export class BaseForm {
    IsLoading = false;
    IsSubmitted = false;

    form: FormGroup;
    get f() { return this.form.controls; }
    onSubmit() {
        throw new Error('Method not implemented.');
    }
}