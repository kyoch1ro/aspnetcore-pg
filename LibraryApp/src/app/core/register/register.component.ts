import { Component, OnInit } from '@angular/core';
import { BaseForm } from '../models/base-form';
import { AccountService } from '../services/account.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertService } from '../services/alert.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent extends BaseForm implements OnInit {



  constructor(private accountSvc: AccountService,
              private fb: FormBuilder,
              private router: Router,
              private alertSrvc: AlertService) {
    super();
  }

  ngOnInit() {
    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
  }
  onSubmit() {
    this.IsLoading = true;
    this.accountSvc
        .Register(this.form.value)
        .pipe(take(1))
        .subscribe(() => {
          this.IsLoading = false;
          this.alertSrvc.CreateNotification('User successfully registered. Please log in to continue');
          this.router.navigate(['/login']);
        },
        err => {
          this.IsLoading = false;
          this.alertSrvc.AlertHttpError(err);
        });
  }

}
