export interface RegisterResource {
    email: string;
    password: string;
    confirmPassword: string;
}