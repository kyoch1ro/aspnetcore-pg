export interface TokenResource {
    token: string;
    expiration: Date;
    refreshToken: string;
}
