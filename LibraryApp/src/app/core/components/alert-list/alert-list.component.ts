import { Component, OnInit, OnDestroy } from '@angular/core';
import { AlertService } from '../../services/alert.service';
import { filter } from 'rxjs/operators';
import { Notification } from '../../models/notification';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-alert-list',
  templateUrl: './alert-list.component.html',
  styleUrls: ['./alert-list.component.css']
})
export class AlertListComponent implements OnInit, OnDestroy {
  notifications: Notification[] = [];
  private subscriptions: Subscription[] = [];
  constructor(private alertSrvc: AlertService) { }

  ngOnInit() {
    this.OnNewNotification();
  }

  private OnNewNotification() {
    const sub = this.alertSrvc
      .Notification$
      .pipe(filter(x => x != null))
      .subscribe(x => {
        this.notifications.push(x);
      });
    this.subscriptions.push(sub);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(x => x.unsubscribe());
  }

  RemoveNotif(item: Notification) {
    const indx = this.notifications.findIndex(x => x.id === item.id);
    if (indx < 0) { return; }
    this.notifications.splice(indx, 1);
  }
}
