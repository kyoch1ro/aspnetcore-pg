import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Notification } from '../../models/notification';
import { NotificationType } from '../../models/notification-type';


@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  @Input() Notification: Notification;
  @Output() Delete: EventEmitter<Notification> = new EventEmitter<Notification>();

  private timer;
  constructor() { }

  ngOnInit() {
    this.timer = setTimeout(() => {
      this.AskForDeletion();
    }, 8000);
  }

  Close() {
    if (this.timer) { clearTimeout(this.timer); }
    this.AskForDeletion();

  }

  private AskForDeletion() {
    this.Delete.next(this.Notification);
  }



}









