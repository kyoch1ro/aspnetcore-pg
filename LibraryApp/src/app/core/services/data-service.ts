import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';
export class DataService<TResource, TKey> {

    constructor(protected endPoint: string, protected http: HttpClient) {
    }

    public Store(resource: TResource): Observable<TResource> {
        return this.http.post(this.endPoint, resource)
                    .pipe(
                        take(1),
                        map(x => x as TResource));
    }

    public Update(key: TKey, resource: TResource): Observable<TResource> {
        return this.http.put(`${this.endPoint}/${key}`, resource)
                        .pipe(
                            take(1),
                            map(x => x as TResource));
    }

    public Get(key: TKey): Observable<TResource> {
        return this.http.get(`${this.endPoint}/${key}`)
                        .pipe(
                            take(1),
                            map(x => x as TResource));
    }

    public Delete(key: TKey): Observable<boolean> {
        return this.http.delete(`${this.endPoint}/${key}`,{responseType: 'text'})
                        .pipe(
                            take(1),
                            map(x => true));
    }

    public List(): Observable<TResource[]> {
        return this.http.get(this.endPoint)
                        .pipe(
                            take(1),
                            map(x => x as TResource[]));
    }



}