import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { RegisterResource } from '../resources/register-resource';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private endPoint = `${environment.webService}/api/account`;
  constructor(private http: HttpClient) { }

  Register(resource: RegisterResource): Observable<boolean> {
    return this.http.post(`${this.endPoint}/register`, resource, {responseType: 'text'}).pipe(
      take(1),
      map(x => true)
    );
  }
}
