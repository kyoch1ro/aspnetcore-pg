import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Notification } from '../models/notification';
import { NotificationType } from '../models/notification-type';
import { PropertyError, HttpErrorHelper } from '../helpers/http-error-helper';
import { HttpErrorResponse } from '@angular/common/http';
import { SplitCamelCaseWithAbbreviations } from '../helpers/string.helper';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  private notification = new BehaviorSubject<Notification>(null);
  public Notification$ = this.notification.asObservable();
  constructor() { }

  public CreateNotification(msg: string, alertType: NotificationType = NotificationType.success) {
    this.notification.next(new Notification({
      message: msg,
      type: alertType
    }));
  }



  public AlertHttpError(err: HttpErrorResponse) {
    const error = HttpErrorHelper.ExtractErrors(err);
    if (error.length) {
      // const property = SplitCamelCaseWithAbbreviations(error[0].property);
      const errors = error.reduce((a, b) => {
        return [...a, ...b.errors];
      } , []);

      let msg = errors[0];
      // console.log(errors);
      if (errors.length > 1) {
        const body = errors.reduce((a, b) =>  a + `<li>${b}</li>` , '');
        msg = `<ul>${body}</ul>`;
      }
      this.CreateNotification(msg, NotificationType.danger);
    }
  }

}
