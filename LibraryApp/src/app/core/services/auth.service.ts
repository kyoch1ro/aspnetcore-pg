import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { TokenResource } from '../resources/token-resource';
import { HttpClient } from '@angular/common/http';
import { take, tap } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { environment } from 'src/environments/environment';

const TOKEN_KEY = 'token';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url =  `${environment.webService}/api/authenticate`;

  private isAuthenticated = new BehaviorSubject<boolean>(false);
  IsAuthenticated$ = this.isAuthenticated.asObservable();
  constructor(private http: HttpClient,
              private cookie: CookieService) { }

  public CachedToken(): TokenResource {
    
    const token = this.cookie.get(TOKEN_KEY);
    if (!token) { return null; }
    return JSON.parse(token) as TokenResource;
  }

  public get IsAuthenticated() {
    return this.isAuthenticated.getValue();
  }

  Login(Username, Password): Observable<any> {
    return this.http.post(`${this.url}/login`, {
      username: Username,
      password: Password
    }).pipe(
      take(1),
      tap((x: TokenResource) => this.SetAuthInfo(x)),
      tap(x => this.isAuthenticated.next(true))
    );
  }

  Logout() {
    this.cookie.deleteAll();
    this.isAuthenticated.next(false);
  }

  private SetAuthInfo(item: TokenResource) {
    this.cookie.set(TOKEN_KEY, JSON.stringify(item));
  }

  RefreshToken(): Observable<TokenResource> {
    const currToken = this.CachedToken();
    return this.http.post(`${this.url}/${currToken.refreshToken}/refresh`, null).pipe(
      take(1),
      tap((x: TokenResource) => this.SetAuthInfo(x))
    );
  }

  CheckCachedToken() {
    if (this.CachedToken()) {
      this.isAuthenticated.next(true);
    }
  }
}
