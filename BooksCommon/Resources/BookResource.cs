﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BooksCommon.Resources
{
    public class BookResource
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Published Date is required")]
        public DateTime? PublishedDate { get; set; }

        public BookResource()
        {
        }
    }
}
