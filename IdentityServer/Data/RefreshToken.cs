﻿namespace IdentityServer.Data
{
    public class RefreshToken
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
        public bool Revoked { get; set; }
    }
}
