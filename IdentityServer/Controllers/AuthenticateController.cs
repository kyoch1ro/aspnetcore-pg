﻿using IdentityServer.Data;
using IdentityServerCommon.Resources;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace IdentityServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticateController : ControllerBase
    {
        private UserManager<ApplicationUser> userManager;
        private ApplicationDbContext _ctx;

        public AuthenticateController(UserManager<ApplicationUser> userManager,
                                     ApplicationDbContext ctx)
        {
            this.userManager = userManager;
            this._ctx = ctx;
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginResource model)
        {
            var user = await userManager.FindByNameAsync(model.Username);
            if (user != null && await userManager.CheckPasswordAsync(user, model.Password))
            {
                return Ok(await GenerateTokenAsync(model.Username));
            }
            return Unauthorized();
        }

        [HttpPost("{refreshToken}/refresh")]
        public async Task<IActionResult> RefreshToken([FromRoute]string refreshToken)
        {
            try
            {
                var updatedRFToken = await UpdateRefreshToken(refreshToken);
                return Ok(await GenerateTokenAsync(updatedRFToken.Username, updatedRFToken.Token));
            }
            catch (Exception ex)
            {
                return Unauthorized(ex.Message);
            }
        }

        private Claim[] GenerateClaim(string username)
        {
            return new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, username),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };
        }
        private SigningCredentials GenerateSigninCredential()
        {
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("m2f312677058a55b31b"));
            return new Microsoft.IdentityModel.Tokens.SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256);

        }
        private async Task<object> GenerateTokenAsync(string username, string refreshToken = null)
        {
            var authClaims = GenerateClaim(username);
            var signingCred = GenerateSigninCredential();
            var token = new JwtSecurityToken(
                   issuer: "http://identity.net",
                   audience: "http://identity.net",
                   expires: DateTime.Now.AddMinutes(30),
                   //expires: DateTime.Now.AddSeconds(20),
                   claims: authClaims,
                   signingCredentials: signingCred);

            return new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token),
                expiration = token.ValidTo,
                refreshToken = refreshToken ?? (await CreateRefreshToken(username)).Token
            };
        }

        private async Task<RefreshToken> CreateRefreshToken(string username)
        {
            var _refreshTokenObj = new RefreshToken
            {
                Username = username,
                Token = Guid.NewGuid().ToString()
            };
            _ctx.RefreshTokens.Add(_refreshTokenObj);
            await _ctx.SaveChangesAsync();
            return _refreshTokenObj;
        }

        private async Task<RefreshToken> UpdateRefreshToken(string refreshToken)
        {
            var tn = await _ctx.RefreshTokens.SingleOrDefaultAsync(x => x.Token == refreshToken);
            if (tn == null) throw new Exception("Token not found.");
            tn.Token = Guid.NewGuid().ToString();
            _ctx.RefreshTokens.Update(tn);
            await _ctx.SaveChangesAsync();
            return tn;
        }


    }
}