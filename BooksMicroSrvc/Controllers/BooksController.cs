﻿using AutoMapper;
using BooksCommon.Resources;
using BooksMicroSrvc.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BooksMicroSrvc.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly AppDbContext _ctx;
        private readonly IMapper _mapper;

        public BooksController(AppDbContext ctx, IMapper mapper)
        {
            this._ctx = ctx;
            this._mapper = mapper;
        }

        

        [HttpGet("")]
        public async Task<IActionResult> GetBooks()
        {
            var books = await _ctx.Books.AsNoTracking().ToListAsync();
            return Ok(_mapper.Map<List<Book>, List<BookResource>>(books));
        }

        [HttpPost("")]
        public async Task<IActionResult> StoreBook([FromBody] BookResource resource)
        {
            var book  = _mapper.Map<BookResource, Book>(resource);
            var bk = _ctx.Books.Add(book);
            await _ctx.SaveChangesAsync();
            return Ok(_mapper.Map<Book, BookResource>(bk.Entity));
        }

        [HttpGet("{bookId}")]
        public async Task<IActionResult> UpdateBook(int bookId)
        {
            var book = await _ctx.Books.AsNoTracking().SingleOrDefaultAsync(x => x.Id == bookId);
            if (book == null) return NotFound();
            return Ok(_mapper.Map<Book, BookResource>(book));
        }


        [HttpPut("{bookId}")]
        public async Task<IActionResult> UpdateBook(int bookId, [FromBody] BookResource resource)
        {
            resource.Id = bookId;
            var book = await _ctx.Books.FindAsync(bookId);
            if (book== null) return NotFound();
            var bk = _mapper.Map<BookResource, Book>(resource);
            _ctx.Entry(book).CurrentValues.SetValues(bk);
            await _ctx.SaveChangesAsync();
            return Ok(_mapper.Map<Book, BookResource>(book));
        }



        [HttpDelete("{bookId}")]
        public async Task<IActionResult> DeleteBook(int bookId)
        {
            var book = await _ctx.Books.FindAsync(bookId);
            if (book == null) return NotFound();
            _ctx.Books.Remove(book);
            await _ctx.SaveChangesAsync();
            return Ok("Data successfully deleted");
        }

    }
}