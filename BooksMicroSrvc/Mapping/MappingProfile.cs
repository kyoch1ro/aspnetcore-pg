﻿using AutoMapper;
using BooksCommon.Resources;
using BooksMicroSrvc.Data;

namespace BooksMicroSrvc.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Book, BookResource>();
            CreateMap<BookResource, Book>();
        }
    }
}
