﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BooksMicroSrvc.Data
{
    public class Book
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        public DateTime PublishedDate { get; set; }
    }
}
