﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BooksMicroSrvc.Migrations
{
    public partial class SeedDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO Books (Name ,PublishedDate) VALUES ('Book 1','12 - 19 - 2019')");
            migrationBuilder.Sql("INSERT INTO Books (Name ,PublishedDate) VALUES ('Book 2','12 - 20 - 2019')");
            migrationBuilder.Sql("INSERT INTO Books (Name ,PublishedDate) VALUES ('Book 3','12 - 21 - 2019')");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM Books");
        }
    }
}
